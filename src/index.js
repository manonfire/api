const express = require("express");
const showdown = require("showdown");
const { join } = require("path");
const fs = require("fs");
const chokidar = require("chokidar");

const app = express();
const converter = new showdown.Converter();

let cache = [];

const watcher = chokidar.watch(join(__dirname, "posts"), {
    ignored: /(^|[\/\\])\../, // ignore dotfiles
    persistent: true
});

watcher
    .on("add", async path => {
        let splits = path.replace(/\.[^/.]+$/, "").split("\\");
        let post = splits[splits.length - 1];

        post = post.match(/([^\/]+$)/)[0];

        await fs.readFile(path, { encoding: "utf8" }, (err, data) => {
            if (err) throw err;
            else {
                let html = converter.makeHtml(data);
                cache.push({
                    title: post.match(/^(.+?)\[/)[1].replace(/\s*$/g, ""),
                    date: post.match(/\[(\d{2}\-\d{1,2}-\d{4})\]/)[1].replace(/\-/g, "/"),
                    timestamp: post.match(/\[(\d{2}\-\d{2})\]/)[1].replace(/\-/g, ":"),
                    char_count: html.length,
                    content: html
                });

                return cache.sort((a, b) => {
                    return new Date(`${b.date} ${b.timestamp}`) - new Date(`${a.date} ${a.timestamp}`)
                });
            }
        });
    })
    .on("unlink", async path => {
        let splits = path.replace(/\.[^/.]+$/, "").split("\\");
        let post = splits[splits.length - 1];

        return cache = cache.filter(c => c.title != post.match(/^(.+?)\[/)[1].replace(/\s*$/g, ""));
    })
    .on("change", async path => {
        let splits = path.replace(/\.[^/.]+$/, "").split("\\");
        let post = splits[splits.length - 1];

        post = post.match(/([^\/]+$)/)[0];

        let orig = cache.filter(c => c.title == post.match(/^(.+?)\[/)[1].replace(/\s*$/g, ""))[0];

        await fs.readFile(path, { encoding: "utf8" }, (err, data) => {
            if (err) throw err;
            else {
                let html = converter.makeHtml(data);
                cache[cache.indexOf(orig)] = {
                    title: post.match(/^(.+?)\[/)[1].replace(/\s*$/g, ""),
                    date: post.match(/\[(\d{2}\-\d{1,2}-\d{4})\]/)[1].replace(/\-/g, "/"),
                    timestamp: post.match(/\[(\d{2}\-\d{2})\]/)[1].replace(/\-/g, ":"),
                    char_count: html.length,
                    content: html
                };

                return cache.sort((a, b) => {
                    return new Date(`${b.date} ${b.timestamp}`) - new Date(`${a.date} ${a.timestamp}`)
                });
            }
        });
    });

app.get("/", (req, res) => {
    return res.json({
        "message": "Beautiful day out isn't it?",
        "version": "0.1.0",
        "posts": cache.length,
        "home": "https://soda.privatevoid.net/axel"
    });
});

app.get("/posts", (req, res) => {
    return res.json({
        "posts": cache
    })
});

app.listen(8083);